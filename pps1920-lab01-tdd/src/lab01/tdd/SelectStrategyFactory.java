package lab01.tdd;

public interface SelectStrategyFactory {

    SelectStrategy createEvenNumberStrategy();

    SelectStrategy createMultipleOfStrategy(int base);

}
