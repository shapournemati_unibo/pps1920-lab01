package lab01.tdd;

public class SelectStrategyFactoryImpl implements SelectStrategyFactory {

    @Override
    public SelectStrategy createEvenNumberStrategy() {
        return new EvenNumberStrategy();
    }

    @Override
    public SelectStrategy createMultipleOfStrategy(int base) {
        return new MultipleOfStrategy(base);
    }

}
