package lab01.tdd;

class MultipleOfStrategy implements SelectStrategy {

    private int baseNumber;

    public MultipleOfStrategy(int baseNumber) {
        this.baseNumber = baseNumber;
    }

    @Override
    public boolean apply(int element) {
        return element == baseNumber;
    }
}
