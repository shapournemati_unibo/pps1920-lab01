package lab01.tdd;

class EvenNumberStrategy implements SelectStrategy {

    @Override
    public boolean apply(int element) {
        return (element % 2) == 0;
    }
}
