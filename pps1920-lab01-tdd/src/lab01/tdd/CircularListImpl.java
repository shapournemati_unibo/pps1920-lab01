package lab01.tdd;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {

    private List<Integer> list;
    private int currentPositionIndicator;

    public CircularListImpl() {
        currentPositionIndicator = 0;
        this.list = new LinkedList<>();
    }

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {

        if (checkEmpty()) return Optional.empty();

        if (currentPositionIndicator >= size()) {
            currentPositionIndicator = 0;
        }
        return Optional.of(list.get(currentPositionIndicator++));
    }

    @Override
    public Optional<Integer> previous() {
        if (checkEmpty()) return Optional.empty();

        if (currentPositionIndicator <= 0) {
            currentPositionIndicator = this.size() - 1;
        } else {
            currentPositionIndicator--;
        }

        return Optional.of((list.get(currentPositionIndicator)));
    }

    @Override
    public void reset() {
        currentPositionIndicator = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {

        if (checkEmpty()) return Optional.empty();

        int startingPositionIndicator = currentPositionIndicator;
        int element;
        do {
             element = next().get();
        } while (!strategy.apply(element) && currentPositionIndicator != startingPositionIndicator);
        if (currentPositionIndicator == startingPositionIndicator) {
            return Optional.empty();
        } else {
            return Optional.of(element);
        }
    }

    private boolean checkEmpty() {
        if (this.isEmpty()) {
            return true;
        }
        return false;
    }
}
