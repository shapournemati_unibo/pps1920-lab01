import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularList circularList;
    SelectStrategyFactory strategyFactory;

    @BeforeEach
    void beforeEach() {
        circularList = new CircularListImpl();
        strategyFactory = new SelectStrategyFactoryImpl();
    }

    @Test
    void testIsInitiallyEmpty() {
        assertTrue(circularList.isEmpty());
    }

    @Test
    void testSimpleAdd() {
        circularList.add(1);
        assertFalse(circularList.isEmpty());
    }

    @Test
    void testSizeInitiallyZero() {
        assertEquals(circularList.size(), 0);
    }

    @Test
    void testSizeAfterAdd() {
        circularList.add(1);
        assertEquals(circularList.size(),1);
    }

    @Test
    void testNextWithOneElement() {
        circularList.add(1);
        assertEquals(circularList.next(), Optional.of(1));
    }

    @Test
    void testNextWithMultipleElements() {
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        circularList.next();
        assertEquals(circularList.next(), Optional.of(2));
    }

    @Test
    void testNextCircularBehaviour() {
        circularList.add(1);
        circularList.add(2);
        circularList.next();
        circularList.next();
        assertEquals(circularList.next(), Optional.of(1));
    }

    @Test
    void testPreviousWithOneElement() {
        circularList.add(1);
        assertEquals(circularList.previous(), Optional.of(1));
    }

    @Test
    void testPreviousWithMultipleElements() {
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        circularList.previous();
        assertEquals(circularList.previous(), Optional.of(2));
    }

    @Test
    void testPreviousCircularBehaviour() {
        circularList.add(1);
        circularList.add(2);
        circularList.previous();
        circularList.previous();
        assertEquals(circularList.next(), Optional.of(1));
    }

    @Test
    void testPreviousAndNextInteraction() {
        circularList.add(1);
        circularList.add(2);
        circularList.next();
        circularList.next();
        circularList.previous();
        assertEquals(circularList.next(), Optional.of(2));
    }

    @Test
    void testResetWithMultipleElements() {
        circularList.add(1);
        circularList.add(2);
        circularList.next();
        circularList.reset();
        assertEquals(circularList.next(), Optional.of(1));
    }

    @Test
    void testNextWithEvenStrategy() {
        circularList.add(1);
        circularList.add(2);
        assertEquals(circularList.next(strategyFactory.createEvenNumberStrategy()),Optional.of(2));
    }

    @Test
    void testNextWithMultipleOfThreeStrategy() {
        circularList.add(1);
        circularList.add(2);
        circularList.add(3);
        assertEquals(circularList.next(strategyFactory.createMultipleOfStrategy(3)), Optional.of(3));
    }

    @Test
    void testNextWithNoElements() {
        assertEquals(circularList.next(), Optional.empty());
    }

    @Test
    void testPreviousWithNoElements() {
        assertEquals(circularList.previous(), Optional.empty());
    }

    @Test
    void testNextWithEvenStrategy_ButNoElements() {
        assertEquals(circularList.next(strategyFactory.createEvenNumberStrategy()), Optional.empty());
    }

}
