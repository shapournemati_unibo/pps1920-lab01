package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    private static final double FEE = 1;

    public SimpleBankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
    }

    public void depositWithAtm(int usrID, double amount) {
        super.deposit(usrID, amount - FEE);
    }

    public void withdrawWithAtm(int usrID, double amount) {
        super.withdraw(usrID, amount + FEE);
    }
}
