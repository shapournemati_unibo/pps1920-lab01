package lab01.example.model;

public interface BankAccountWithAtm extends BankAccount {

    public void depositWithAtm(int usrID, double amount);

    public void withdrawWithAtm(int usrID, double amount);

}
